import Vue from 'vue'
import VueRouter from 'vue-router'
import List from '@/components/List.vue'
import EmptyList from '@/components/EmptyList.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/List',
    name: 'List',
    component: List,
  },
  {
    path: '/EmptyList',
    name: 'EmptyList',
    component: EmptyList
  }
]

const router = new VueRouter({
  routes
})

export default router
