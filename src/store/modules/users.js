const state = {
    users: [],
    currentUser: null
  };
  
  const getters = {
    users: (state) => state.users,
    currentUser: (state) => state.currentUser
  };
  
  const mutations = {
    setUsers(state, users) {
      state.users = users;
    },
    setCurrentUser(state, user) {
      state.currentUser = user;
    },
    addUser(state, newUser) {
      state.users.push(newUser);
    },
    removeUser(state, userId) {
      state.users = state.users.filter(user => user.id !== userId);
    },
  };
  
  export default {
    state,
    getters,
    mutations
  };