const state = {
  items: [],
};

const getters = {
  items: (state) => state.items,
  userItemExists: (state) => (id) => state.items.some(i => i.userId === id),
};

const mutations = {
  addItem(state, newName) {
    state.items.push(newName);
  },
  removeItem(state, itemId) {
    state.items = state.items.filter(item => item.id !== itemId);
  },
  setItems(state, items) {
    state.items.push(items);
  },
};

export default {
  state,
  getters,
  mutations
};