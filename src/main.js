import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  async created() {
    const userResponse = await fetch('https://jsonplaceholder.typicode.com/users');
    const user = await userResponse.json();
    this.$store.commit('setUsers', user);
  },
  router,
  store,
  render: h => h(App)
}).$mount('#app')
